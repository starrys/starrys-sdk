Starrys PHP SDK
======
![screenshot of sample](http://starrys.ru/img/logo-inline.svg)

SDK для работы с API облачного сервиса [Чек онлайн СТАРРУС](http://check.starrys.ru)

* [Cloud API Сценарий работы с устройством, арендованном в «облачном» сервисе для устройств версии 3.3.10](http://check.starrys.ru/docs/cloudapi_complex.pdf)

* [Протокол взаимодействия с ККТ версии 3.10.0. JSON API](http://check.starrys.ru/docs/protocol.pdf)

Отлично подходит для любых интернет магазинов.
## Примеры использования
```php
use Starrys\Cashbox\Api;
use Starrys\Cashbox\Commands\ComplexCommand;
use Starrys\Cashbox\Line;
use Starrys\Cashbox\DocumentType;
use Starrys\Cashbox\PayAttribute;
    
$host = 'https://fce.starrys.ru:4443';
$api = new Api($host);
$api->setClientName('custom');
$api->setClientVer('1.0');
$api->setCertificate('cert', 'cert_key', 'cert_password');
    
$command = new ComplexCommand('requestId', 'clientId');
$command->setPhoneOrEmail('customer@example.com')
    ->setNonCash(array(0,10000,0))
    ->setTaxMode(pow(2, 0))
    ->setMaxDocumentsInTurn(20)
    ->setGroup('')
    ->setDocumentType(DocumentType::DEBIT)
    ->setPassword(1)
    ->setFullResponse(false)
    ->setDevice('auto');
    
$line = new Line();
$line->setDescription('Товар')
    ->setPayAttribute(PayAttribute::FULL_PAYMENT)
    ->setPrice(10000)
    ->setQty(1000)
    ->setTaxId(2);
    
$command->addLine($line);
    
$result = $api->executeCommand($command);
    
$request = $result->getRequest();
```
## Установка
<pre><code>composer require starrys/starrys-sdk</code></pre>