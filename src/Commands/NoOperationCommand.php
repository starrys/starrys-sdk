<?php

namespace Starrys\Cashbox\Commands;

use Starrys\Cashbox\Exceptions\StarrysCommandException;

class NoOperationCommand extends BaseCommand
{
	const NAME = 'NoOperation';

	/**
	 * @var int|null
	 */
	private $Password;

	public function getParams()
	{
		return get_object_vars($this);
	}

	/**
	 * @param int $Password
	 * @return NoOperationCommand
	 */
	public function setPassword($Password)
	{
		$this->Password = $Password;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getPassword()
	{
		return $this->Password;
	}

    /**
     * {@inheritdoc}
     */
	public function validate()
    {
        parent::validate();
        if (isset($this->Password) == false) {
            throw new StarrysCommandException('The password must be determined', $this);
        }
    }
}