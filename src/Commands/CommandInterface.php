<?php

namespace Starrys\Cashbox\Commands;

interface CommandInterface
{
	/**
	 * @return array
	 */
	public function getParams();

    /**
     * @throws \Starrys\Cashbox\Exceptions\StarrysCommandException
     */
	public function validate();
}