<?php

namespace Starrys\Cashbox;

use Starrys\Cashbox\HttpClients\CurlHttpClient;

class StarrysClient
{
	/**
	 * @const string Starrys API path
	 */
	const API_PATH = '/fr/api/v2/';

	/**
	 * @var \Starrys\Cashbox\HttpClients\HttpClientInterface|null HTTP Client
	 */
	protected $httpClientHandler;

	/**
	 * Inital a new StarrysClient object
	 *
	 * @param \Starrys\Cashbox\HttpClients\HttpClientInterface|null $httpClientHandler
	 */
	function __construct($httpClientHandler = null)
	{
		$this->httpClientHandler = $httpClientHandler ?: new CurlHttpClient();
	}

	/**
	 * Returns the HTTP client handler.
	 *
	 * @return \Starrys\Cashbox\HttpClients\HttpClientInterface
	 */
	public function getHttpClientHandler()
	{
		return $this->httpClientHandler;
	}

	/**
	 * @return string
	 */
	public function getApiPath()
	{
		return static::API_PATH;
	}

	/**
	 * @param StarrysRequest $request
	 * @return StarrysResponse
	 */
	public function sendRequest($request)
	{
		$rawResponse = $this->httpClientHandler->send(
			$request->getCertPath(),
			$request->getKeyPath(),
			$request->getCertPassword(),
			$request->getHost() . $this->getApiPath() . $request->getEndpoint(),
			$request->getMethod(),
			$request->getHeaders(),
			$request->getParams(),
			$request->getTimeout()
		);
		$response = new StarrysResponse($request, $rawResponse);
		return $response;
	}

}