<?php

namespace Starrys\Cashbox\Exceptions;


class StarrysLineException extends StarrysSDKException
{
    /**
     * @var \Starrys\Cashbox\Line
     */
    private $receiptLine;


    /**
     * StarrysLineException constructor.
     * @param string $message
     * @param \Starrys\Cashbox\Line $receiptLine
     */
    public function __construct($message = "", $receiptLine = null)
    {
        $this->receiptLine = $receiptLine;
        parent::__construct($message);
    }

    /**
     * @return \Starrys\Cashbox\Line
     */
    public function getReceiptLine()
    {
        return $this->receiptLine;
    }

}