<?php

namespace Starrys\Cashbox\Exceptions;


class StarrysCommandException extends StarrysSDKException
{
    /**
     * @var \Starrys\Cashbox\Commands\BaseCommand
     */
    private $command;


    /**
     * StarrysLineException constructor.
     * @param string $message
     * @param \Starrys\Cashbox\Commands\BaseCommand $command
     */
    public function __construct($message = "", $command = null)
    {
        $this->command = $command;
        parent::__construct($message);
    }

    /**
     * @return \Starrys\Cashbox\Commands\BaseCommand
     */
    public function getCommand()
    {
        return $this->command;
    }

}